# Tp 1 : Réseaux

# I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

#### 1.a 
La commande utilisée est
```ìpconfig /all```

Interface WiFi : 
```   
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 1C-BF-C0-17-32-09
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::18de:b4d7:aedf:259a%8(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.133(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 10:41:38
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 12:41:39
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 169656256
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-28-2C-24-B5-1C-BF-C0-17-32-09
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
Le nom est "Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC"
L'adresse mac est "1C-BF-C0-17-32-09"
L'adresse ip est "10.33.0.133"

Interface Ethernet : 
```
Carte Ethernet Connexion réseau Bluetooth :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Adresse physique . . . . . . . . . . . : 1C-BF-C0-17-32-0A
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
```
Le nom est "Bluetooth Device (Personal Area Network)"
L'adresse mac est "1C-BF-C0-17-32-0A"

#### 1.b
La commande utilisée est
```ìpconfig```

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::18de:b4d7:aedf:259a%8
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.133
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

``` 
L'adresse IP de la passerelle de ma carte WiFi est 10.33.3.253

#### 1.c
![](https://i.imgur.com/TD42Eih.png)

Il faut d'abord suivre les étapes en rouge. Entrée le chemin donné dans le panneau de configurration (1), cliqué sur la connexions wifi (2) et cliqué sur détails (3)

En bleu nous avons les différentes informations, en 1 l'adresse mac, en 2 l'adresse ip et en 3 la gateway

#### 1.d
La gateway dans le réseau ynov permet de se connecter à Internet

### 2. Modifications des informations

#### 2.A
![](https://i.imgur.com/PF8Mmfh.png)

Pour modifier son ip avec l'interface graphique, il faut cliqué sur boutons entouré par les carrés rouge dans le screen (1,2,3) et en 4 entrer la nouvelle ip ainsi que le masque et la passerelle par défaut

Je n'ai pas accés a internet aprés avoir changé mon ip, l'ip est peut etre deja prise

#### 2.B : Table ARP

```
C:\Users\adrie>arp /a

Interface : 10.33.0.133 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.140.1 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.140.254       00-50-56-e4-28-af     dynamique
  192.168.140.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.232.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  192.168.232.254       00-50-56-e8-a4-ab     dynamique
  192.168.232.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

L'adresse mac de la passerelle de mon réseau est "00-12-00-40-4c-bf", pour la trouver j'ai d'abord chercher l'addresse ip de la passerelle puis j'ai récupéré l'addresse mac correspondante 

#### Et si on remplissait un peu la table ?

```
C:\Users\adrie>ping 10.33.0.0

Envoi d’une requête 'Ping'  10.33.0.0 avec 32 octets de données :
Réponse de 10.33.0.133 : Impossible de joindre l’hôte de destination.
Réponse de 10.33.0.133 : Impossible de joindre l’hôte de destination.
Réponse de 10.33.0.133 : Impossible de joindre l’hôte de destination.

Statistiques Ping pour 10.33.0.0:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Ctrl+C
^C
C:\Users\adrie>ping 10.33.3.255

Envoi d’une requête 'Ping'  10.33.3.255 avec 32 octets de données :
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255

Statistiques Ping pour 10.33.3.255:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 3ms, Moyenne = 3ms

C:\Users\adrie>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 20ms, Moyenne = 18ms
```
On envoie des ping

```
C:\Users\adrie>arp /a

Interface : 10.33.0.133 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.33.2.62            b0-7d-64-b1-98-d3     dynamique
  10.33.2.208           a4-b1-c1-72-13-98     dynamique
  10.33.3.33            c8-58-c0-63-5a-92     dynamique
  10.33.3.81            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
 
 [...]
```
Les address ip que j'ai ping ainsi que leur addresse mac : 
ip  10.33.2.62    mac :        b0-7d-64-b1-98-d3     
ip  10.33.3.33    mac :   c8-58-c0-63-5a-92     
ip  10.33.3.81    mac :   3c-58-c2-9d-98-38     

### 2.C : Nmap
Résultat Nmap : 
```
C:\Users\adrie>nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 09:16 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.057s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
```
![](https://c.tenor.com/Bar_E2hAIzQAAAAC/time-timecard.gif)
```
[...]

Host is up (0.028s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.0.133
Host is up.
Nmap done: 1024 IP addresses (85 hosts up) scanned in 25.93 seconds
```
Ceci est le scan de ping sur le réseau YNOV

```
C:\Users\adrie>arp /a

Interface : 10.33.0.133 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.33.0.43            2c-8d-b1-94-38-bf     dynamique
  10.33.0.60            e2-ee-36-a5-0b-8a     dynamique
  10.33.0.96            ca-4f-f4-af-8f-0c     dynamique
  10.33.0.135           f8-5e-a0-06-40-d2     dynamique
  10.33.1.166           1a-41-0b-54-a5-a0     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.3.59            02-47-cd-3d-d4-e9     dynamique
  10.33.3.80            3c-58-c2-9d-98-38     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```
Ceci est ma table arp apres le ping, on y voit donc les différentes qui sont prises

![](https://i.imgur.com/3QMgMwZ.png)
Changement de l'ip

```
C:\Users\adrie>ipconfig

Configuration IP de Windows

[...]

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::18de:b4d7:aedf:259a%8
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.5
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]

C:\Users\adrie>ping 10.33.3.253

Envoi d’une requête 'Ping'  10.33.3.253 avec 32 octets de données :
Réponse de 10.33.3.253 : octets=32 temps=15 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=41 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=43 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=21 ms TTL=255

Statistiques Ping pour 10.33.3.253:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 15ms, Maximum = 43ms, Moyenne = 30ms

C:\Users\adrie>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 21ms, Moyenne = 19ms

C:\Users\adrie>ping google.com

Envoi d’une requête 'ping' sur google.com [216.58.214.174] avec 32 octets de données :
Réponse de 216.58.214.174 : octets=32 temps=55 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=30 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=45 ms TTL=114
Réponse de 216.58.214.174 : octets=32 temps=28 ms TTL=114

Statistiques Ping pour 216.58.214.174:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 28ms, Maximum = 55ms, Moyenne = 39ms
```

# II. Exploration locale en duo
Je n'ai pas de port RJ45 sur mon pc, j'ai donc travaillé avec un groupe qui en possédais 

### 1. Modification d'adresse IP

Configuration des machines :
Le premier aura 192.168.0.1 et le 2ème 192.168.0.2.
![](https://i.imgur.com/ods44w3.png)

Les changements ont bien pris effet :
```
PS C:\Users\DIRECTEUR_PC2> ipconfig

Configuration IP de Windows
[...]
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::f166:4347:db54:1052%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
[...]
```

Ping de la 2ème machine :
```
PS C:\Users\DIRECTEUR_PC2> ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 2ms, Moyenne = 1ms
```
### 2. Utilisation d'un des deux comme gateway

Depuis la 2ème machine :

Ping 8.8.8.8 ->
```
PS C:\Users\nicol> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=34 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 25ms, Maximum = 34ms, Moyenne = 29ms
```

tracert vers www.google.com ->
```
PS C:\Users\nicol> tracert www.google.com

Détermination de l’itinéraire vers www.google.com [142.250.179.68]
avec un maximum de 30 sauts :

  1     3 ms     2 ms     2 ms  DIRECTEUR-PC2 [192.168.0.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     4 ms     5 ms     4 ms  10.33.3.253
  4     5 ms     6 ms     7 ms  10.33.10.254
  5     4 ms     3 ms     4 ms  reverse.completel.net [92.103.174.137]
  6    11 ms     9 ms     8 ms  92.103.120.182
  7    21 ms    19 ms    21 ms  172.19.130.113
  8    19 ms    19 ms    19 ms  46.218.128.78
  9    21 ms    21 ms    21 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 10    22 ms    21 ms    22 ms  186.144.6.194.rev.sfr.net [194.6.144.186]
 11    22 ms    22 ms    23 ms  72.14.194.30
 12    23 ms    22 ms    22 ms  108.170.231.111
 13    22 ms    21 ms    21 ms  142.251.49.133
 14    21 ms    21 ms    21 ms  par21s19-in-f4.1e100.net [142.250.179.68]

Itinéraire déterminé.
```

### 3. Petit chat privé

Serveur :
```
PS C:\Users\DIRECTEUR_PC2\Desktop\Ynov> .\nc.exe -l -p 8888
Salut !
Hola ! Ca va ?
Super et toi ?
Je sais pas mettre de GIF dans ce truc... Donc ca va pas !
gifdechat.gif
=(
```

Client :
```
PS C:\Users\nicol\Downloads> .\nc.exe 192.168.0.1 8888
Salut !
[...]
```
Si on veut préciser sur quelle ip écouter (en mode serveur) : `.\nc.exe -l -p 8888 192.168.0.2`

### 4. Firewall

Pour activer les ping, il faut autoriser les règles `File and Printer Sharing (Echo Request - ICMPv4-In)` dans le pare feu.
Pour NETCAT, il faut ajouter une règle avec le programme et ajouter les ports voulus.

![](https://i.imgur.com/xy1TLTH.png)

![](https://i.imgur.com/A8WW3Kt.png)

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

```
C:\Users\adrie>ipconfig /all

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Realtek 8822CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 1C-BF-C0-17-32-09
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::18de:b4d7:aedf:259a%8(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.0.133(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 11:37:16
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 13:37:16
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 169656256
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-28-2C-24-B5-1C-BF-C0-17-32-09
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
L'adresse ip du serveur DHCP est 10.33.3.254
Sa date d'expiration est le "jeudi 16 septembre 2021 13:37:16"

## 1. DNS

Avec la commande précédente, on voit qu'il y a 3 ip dz serveur DNS qui sont 10.33.10.2,10.33.10.148 et 10.33.10.155

```
C:\Users\adrie>nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80c::200e
          142.250.178.142


C:\Users\adrie>nslookup ynov.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
L'adresse ip de google.com est 142.250.178.142, celle de ynov.com est 92.243.16.143

L'ip du serveur auquel les requetes on était effectués est 10.33.10.2 , ip qui correspond à l'ip d'un des 3 serveurs DNS

```
C:\Users\adrie>nslookup 78.74.21.21
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21


C:\Users\adrie>nslookup 92.146.54.88
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
Le nom de domaine de l'ip 78.74.21.21 est host-78-74-21-21.homerun.telia.com, celui de l'ip 92.146.54.88 est apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr

# IV. Wireshark
un ping entre moi et la passerelle
![](https://i.imgur.com/ywkDLzF.png)

un netcat entre moi et votre mate, branché en RJ45
![](https://i.imgur.com/VH6cpFF.png)

Une requête DNS
![](https://i.imgur.com/A3IwLd9.png)
Le serveur au quel est fait le demande est le serveur avec l'ip 10.33.10.2

