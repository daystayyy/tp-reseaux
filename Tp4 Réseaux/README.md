# TP4 : Vers un réseau d'entreprise
## I. Dumb switch
### 3. Setup topologie 1

On change l'ip du pc1
```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

On change l'ip du pc2
```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```
On utilise la commande `write` pour utiliser la nouvelle ip à chaque redémarrage 

On ping pc2 depuis pc1
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.394 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.045 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=1.934 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=1.716 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=2.156 ms
```

## II. VLAN
### 3. Setup topologie 2

On change l'ip du pc3
```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```

Toutes les machines se ping

Pc1
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=2.601 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.621 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.380 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=2.011 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=1.739 ms

PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=3.130 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=1.128 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=2.117 ms
84 bytes from 10.1.1.3 icmp_seq=4 ttl=64 time=2.116 ms
84 bytes from 10.1.1.3 icmp_seq=5 ttl=64 time=1.665 ms
```

Pc2
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=3.565 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=3.120 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=3.394 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.890 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=3.760 ms

PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=1.798 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=1.116 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=1.312 ms
84 bytes from 10.1.1.3 icmp_seq=4 ttl=64 time=1.433 ms
84 bytes from 10.1.1.3 icmp_seq=5 ttl=64 time=2.367 ms
```

Pc3
```
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.666 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.435 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=3.286 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=3.121 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=3.237 ms

PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=3.461 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=4.445 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=3.646 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=4.126 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=3.555 ms
```

Configuration des VLANs

Sur le switch : 
On créé les 2 vlans
```
#on passe en mode privilégié  
Switch>enable

#on passe en mode configuration
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.

#on créé le vlan 10 avec pour nom admins
Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#

#on créé le vlan 20 avec pour nom guests
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit
Switch(config)#exit
```

On affiche les vlans
```
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active
20   guests                           active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
10   enet  100010     1500  -      -      -        -    -        0      0
20   enet  100020     1500  -      -      -        -    -        0      0
1002 fddi  101002     1500  -      -      -        -    -        0      0
1003 tr    101003     1500  -      -      -        -    -        0      0
1004 fdnet 101004     1500  -      -      -        ieee -        0      0
1005 trnet 101005     1500  -      -      -        ibm  -        0      0

Remote SPAN VLANs
------------------------------------------------------------------------------


Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------
    
```
On retrouve nos deux vlans avec ces lignes 
```
10   admins                           active
20   guests                           active
```

ajout des ports du switches dans le bon VLAN
```
# interface de pc1
Switch(config)#interface gigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit

# interface de pc2
Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit



# interface de pc3
Switch(config)#interface gigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit

# On vérifie 
Switch(config)#exit
Switch#show vlan br
*Oct 18 11:40:38.401: %SYS-5-CONFIG_I: Configured from console by console

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```
On voit que ca a marché grace à ces deux lignes
```
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
```

Vérif

pc1 et pc2 ce ping 
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=3.021 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.334 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=2.445 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=2.193 ms
84 bytes from 10.1.1.2 icmp_seq=5 ttl=64 time=2.054 ms
```
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=7.443 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=4.153 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=4.021 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=3.242 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=11.511 ms
```

Mais ne peuvent pas ping pc3 et inversement
```
PC2> ping 10.1.1.3

host (10.1.1.3) not reachable
```
```
PC1> ping 10.1.1.3

host (10.1.1.3) not reachable
```

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

## III. Routing

Adressage ip 


```
pc1> show ip

NAME        : pc1[1]
IP/MASK     : 10.1.1.1/24
[...]
```
```
pc2> show ip

NAME        : pc2[1]
IP/MASK     : 10.1.1.2/24
[...]
```
```
adm1> show ip

NAME        : PC3[1]
IP/MASK     : 10.2.2.1/24
```

```
[adrien@web1 ~]$ ip a
[...]
2: enp0s3: [...]
    inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
[...]
```



On créé les 3 vlans 
```
Switch#conf t

Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit

Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit

Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
```

On donne attribué les vlans aux interfaces
```
Switch(config)#interface gigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit

Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit

Switch(config)#interface gigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit


Switch(config)#interface gigabitEthernet0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
```

Ajout du routeur comme un trunk
```
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#interface gigabitEthernet1/1
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/1       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/1       1-4094

Port        Vlans allowed and active in management domain
Gi1/1       1,10-13,20

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/1       none
```

 Config du routeur
```
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit

R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit

R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
```
On up notre interface
```
R1(config)#interface fastEthernet 0/0
R1(config-subif)# no shutdown
```

Vérif

ping vers le router

pc1
```
pc1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.669 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=6.716 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=16.173 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=13.868 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=4.600 ms
```

pc2
```
pc2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=9.624 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=11.108 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=5.996 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=6.700 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=8.792 ms
```

adm1
```
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=9.883 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=5.362 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=3.117 ms
84 bytes from 10.2.2.254 icmp_seq=4 ttl=255 time=7.314 ms
84 bytes from 10.2.2.254 icmp_seq=5 ttl=255 time=8.360 ms

```

web1
```
[adrien@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=10.9 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=23.4 ms
64 bytes from 10.3.3.254: icmp_seq=3 ttl=255 time=14.10 ms

--- 10.3.3.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 10.915/16.443/23.449/5.223 ms

```

On donne à nos pc l'adresse de la gateway
Pour les pcs : 
```
pc1> ip 10.1.1.1 10.1.1.254
```
```
pc2> ip 10.1.1.2 10.1.1.254
```
```
adm1> ip 10.2.2.1 10.2.2.254
```
Pour la vm web1 on ajoute la ligne suivant dans le fichier ifcfg-enp0s3 contenue dans /etc/sysconfig/network-scripts/
```
GATEWAY=10.3.3.254
```

Ping de pc1 vers adm1 et web1
```
pc1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=33.550 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=28.451 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=42.532 ms
84 bytes from 10.2.2.1 icmp_seq=4 ttl=63 time=33.617 ms
84 bytes from 10.2.2.1 icmp_seq=5 ttl=63 time=20.285 ms

pc1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=15.804 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=38.330 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=28.802 ms
84 bytes from 10.3.3.1 icmp_seq=4 ttl=63 time=17.299 ms
84 bytes from 10.3.3.1 icmp_seq=5 ttl=63 time=31.534 ms
```
Ping de web1 vers pc2 et adm1
```
[adrien@web1 ~]$ ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=26.3 ms
64 bytes from 10.1.1.2: icmp_seq=3 ttl=63 time=36.0 ms
64 bytes from 10.1.1.2: icmp_seq=4 ttl=63 time=34.4 ms

--- 10.1.1.2 ping statistics ---
4 packets transmitted, 3 received, 25% packet loss, time 3047ms
rtt min/avg/max/mdev = 26.346/32.262/36.037/4.238 ms


[adrien@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=17.6 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=63 time=21.8 ms
64 bytes from 10.2.2.1: icmp_seq=3 ttl=63 time=34.2 ms
64 bytes from 10.2.2.1: icmp_seq=4 ttl=63 time=28.1 ms

--- 10.2.2.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 17.574/25.448/34.238/6.316 ms
```

## IV. NAT

### 3. Setup topologie 4

On récupére une ip en dhcp
```
R1#conf t
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
```

On peut ping 1.1.1.1
Depuis web1
```
[adrien@web1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=3 ttl=62 time=24.10 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=62 time=31.10 ms
64 bytes from 1.1.1.1: icmp_seq=5 ttl=62 time=33.5 ms
64 bytes from 1.1.1.1: icmp_seq=6 ttl=62 time=33.7 ms

--- 1.1.1.1 ping statistics ---
6 packets transmitted, 4 received, 33.3333% packet loss, time 5085ms
rtt min/avg/max/mdev = 24.950/31.043/33.725/3.586 ms
```
Depuis pc1
```
pc1> ping 1.1.1.1

84 bytes from 1.1.1.1 icmp_seq=1 ttl=62 time=25.611 ms
84 bytes from 1.1.1.1 icmp_seq=2 ttl=62 time=28.575 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=62 time=30.613 ms
84 bytes from 1.1.1.1 icmp_seq=4 ttl=62 time=25.670 ms
84 bytes from 1.1.1.1 icmp_seq=5 ttl=62 time=21.800 ms
```

Configurez le NAT
```
R1(config)#interface fastEthernet 0/0
R1(config-if)#ip nat inside
R1(config-if)#exit

R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside
R1(config-if)#exit

R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
```

Test

configurez l'utilisation d'un DNS

Sur les VPCS on utilise la commande suivante
```
ip dns <ip du dns>

ex: 
ip dns 1.1.1.1
``` 

Sur la machine Linux on ajoute la ligne suivant dans le fichier ifcfg-enp0s3 contenue dans /etc/sysconfig/network-scripts/
```
DNS1=1.1.1.1
```

Ping depuis pc1 
```
pc1> ping google.com
google.com resolved to 172.217.18.206

84 bytes from 172.217.18.206 icmp_seq=1 ttl=112 time=40.898 ms
84 bytes from 172.217.18.206 icmp_seq=2 ttl=112 time=36.592 ms
84 bytes from 172.217.18.206 icmp_seq=3 ttl=112 time=34.937 ms
84 bytes from 172.217.18.206 icmp_seq=4 ttl=112 time=44.777 ms
84 bytes from 172.217.18.206 icmp_seq=5 ttl=112 time=42.546 ms
```

Ping depuis la machine linux 
```
[adrien@web1 ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=39.5 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=38.1 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=115 time=36.5 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=4 ttl=115 time=46.8 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 36.463/40.214/46.799/3.949 ms
```

## V. Add a building