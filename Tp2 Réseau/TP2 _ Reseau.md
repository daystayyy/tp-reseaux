# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

ping de la machine 1 vers la machine 2 : 
```
[adrien@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.462 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.617 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.590 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.575 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3100ms
rtt min/avg/max/mdev = 0.462/0.561/0.617/0.059 ms
```

ping de la machine 2 vers la machine 1 :

```
[adrien@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.497 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.697 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.821 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2033ms
rtt min/avg/max/mdev = 0.497/0.671/0.821/0.136 ms
```

Table Arp de la premiere machine : 
```
[adrien@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:64 DELAY
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.12 dev enp0s8 lladdr 08:00:27:d6:00:b2 STALE

```

Table Arp de la deuxieme machine : 
```
[adrien@node2 ~]$ ip neigh show
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:37:96:3b STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:64 REACHABLE
```

L'adresse mac de node1 est 08:00:27:37:96:3b
L'adresse mac de node2 est 08:00:27:d6:00:b2

Adresse mac de node2 : 
```
[adrien@node2 ~]$ ip a
[...]
3: enp0s8: 
[...]
    link/ether 08:00:27:d6:00:b2 
[...]
```

### 2. Analyse de trames

On vide les tables arp : 
```
[adrien@node1 ~]$ sudo ip neigh flush all
```
```
[adrien@node2 ~]$ sudo ip neigh flush all
```


Capture de trame depuis la premiere machine
```
[adrien@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C47 packets captured
49 packets received by filter
0 packets dropped by kernel
[adrien@node1 ~]$ ls
tp2_arp.pcap
```
ping de la machine 2 vers la machine 1 peadant que la premiere capture :
```
[adrien@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.446 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.590 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.702 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.512 ms
64 bytes from 10.2.1.11: icmp_seq=5 ttl=64 time=0.629 ms
64 bytes from 10.2.1.11: icmp_seq=6 ttl=64 time=0.542 ms

64 bytes from 10.2.1.11: icmp_seq=7 ttl=64 time=0.584 ms
^C
--- 10.2.1.11 ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6168ms
rtt min/avg/max/mdev = 0.446/0.572/0.702/0.077 ms
```


| ordre | type trame | source | destination |
| -------- | -------- | -------- | -------- |
| 1 | Requête ARP | node2 08:00:27:d6:00:b2    | Broadcast FF:FF:FF:FF:FF     |
| 2 | Requête ARP | node2 08:00:27:d6:00:b2    | Broadcast FF:FF:FF:FF:FF     |
| 3 | Requête ARP | node1 08:00:27:37:96:3b | node2 08:00:27:d6:00:b2      |
| 4 | Requête ARP | node1 08:00:27:37:96:3b | node2 08:00:27:d6:00:b2      |
| 5 | Requête ARP | node2 08:00:27:d6:00:b2     |  node1 08:00:27:37:96:3b     |

## II. Routage

### 1. Mise en place du routage
 
Activation du routage sur le noeud router.net2.tp2

```
[adrien@router ~]$ sudo firewall-cmd --list-all
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[adrien@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[adrien@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[adrien@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
[adrien@router ~]$
```

Ping de node1 vers Marcel : 
```
[adrien@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.676 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.828 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.684 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2045ms
rtt min/avg/max/mdev = 0.676/0.729/0.828/0.073 ms
```

Ping de marcel vers node1
```
[adrien@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.751 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.691 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=0.565 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2073ms
rtt min/avg/max/mdev = 0.565/0.669/0.751/0.077 ms
```


### 2. Analyse de trames
Premier ping : 

Table Arp du routeur : 
```
[adrien@router ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:64 DELAY
10.2.2.12 dev enp0s9 lladdr 08:00:27:47:0c:ee STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:d6:00:b2 STALE
```
Table Arp de Node1 : 
```
[adrien@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:64 REACHABLE
10.2.1.254 dev enp0s8 lladdr 08:00:27:37:96:3b REACHABLE
```

Table Arp de Marcel : 
```
[adrien@marcel ~]$ ip neigh show
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:70 REACHABLE
10.2.2.254 dev enp0s8 lladdr 08:00:27:06:4a:d1 REACHABLE
```

Deuxieme ping : 

Node 1 : 
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.11         | `node1` `08:00:27:d6:00:b2`  | x             | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.2.1.254         | `router` `08:00:27:37:96:3b` | 10.2.1.11              | `node1` `08:00:27:d6:00:b2`   |
| 3   | Requête ARP         | 10.2.2.254       |`routeur` `08:00:27:06:4a:d1`                     | x              |          Broadcast `FF:FF:FF:FF:FF`                  |
| 4   | Réponse ARP         | 10.2.2.12       | `marcel` `08:00:27:47:0c:ee`                      |  10.2.2.254              |  `routeur` `08:00:27:06:4a:d1`                           |
| 5   | Requête ARP         | 10.2.1.254      |`router` `08:00:27:37:96:3b`                      | x               |   Broadcast `FF:FF:FF:FF:FF`                          |
| 6   | Réponse ARP           | 10.2.1.11      | `node1` `08:00:27:d6:00:b2`                      |   10.2.1.254             |  `router` `08:00:27:37:96:3b`                          |
| 7   | Requête ARP         | 10.2.2.12      | `marcel` `08:00:27:47:0c:ee`                         |    x            |       Broadcast `FF:FF:FF:FF:FF`                      |
| 8   |  Réponse ARP         | 10.2.2.254       | `routeur` `08:00:27:06:4a:d1`                       | 10.2.2.12               |   `marcel` `08:00:27:47:0c:ee`                          |
| 9     | Ping        | 10.2.1.11         | `node1` `08:00:27:d6:00:b2`                         | 10.2.2.12             | `router` `08:00:27:37:96:3b`                            |
| 10     | Pong        | 10.2.2.12         | `router` `08:00:27:37:96:3b`                         | 10.2.1.11             | `node1` `08:00:27:d6:00:b2`                          |


### 3. Accès internet

Ping internet 

Node1 : 
```
[adrien@node1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=52 time=31.1 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=52 time=32.4 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 31.102/31.730/32.359/0.653 ms
```

Marcel : 
```
[adrien@marcel ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=52 time=47.3 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=52 time=29.1 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 29.079/38.190/47.301/9.111 ms
```

Test du serveur dns 

Node1 : 

```
[adrien@node1 ~]$ dig gitlab.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32188
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             91      IN      A       172.65.251.78

;; Query time: 49 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 16:33:32 CEST 2021
;; MSG SIZE  rcvd: 55

```
```
[adrien@node1 ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=52 time=40.4 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=52 time=34.1 ms
^C
--- gitlab.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 34.058/37.207/40.357/3.155 ms
```

Marcel : 

```
[adrien@marcel ~]$ dig gitlab.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62285
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             275     IN      A       172.65.251.78

;; Query time: 41 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 16:35:26 CEST 2021
;; MSG SIZE  rcvd: 55

```

```
[adrien@marcel ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=52 time=32.0 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=52 time=37.2 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=3 ttl=52 time=27.9 ms
^C
--- gitlab.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 27.865/32.366/37.204/3.820 ms
```


Analyse de trames

Node 1 : 
| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping| 10.2.1.11         | `node1` `08:00:27:d6:00:b2`  | 8.8.8.8             | `router` `08:00:27:37:96:3b` |
| 2     | Pong| 8.8.8.8        | `router` `08:00:27:37:96:3b` | 10.2.1.11              | `node1` `08:00:27:d6:00:b2`   |

## III. DHCP


### 1. Mise en place du serveur DHCP

`ip a` de node2 apres avoir récupérer une ip sur le serveur dhcp : 


```
[adrien@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6b:fc:15 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.3/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 734sec preferred_lft 734sec
    inet6 fe80::a00:27ff:fe6b:fc15/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
Ping de sa gateway
```
[adrien@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.436 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.531 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1028ms
rtt min/avg/max/mdev = 0.436/0.483/0.531/0.052 ms
```

Sa route : 
```
[adrien@node2 ~]$ ip route show
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.3 metric 100
```

Ping marcel
```
[adrien@node2 ~]$ ip route show
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.3 metric 100
```

Il connais l'adresse d'un serveur DNS : 
```
[adrien@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34881
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             21      IN      A       142.250.75.238

;; Query time: 134 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sat Sep 25 19:34:02 CEST 2021
;; MSG SIZE  rcvd: 55

```

```
[adrien@node2 ~]$ ping google.com
PING google.com (216.58.198.206) 56(84) bytes of data.
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=1 ttl=111 time=33.2 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=2 ttl=111 time=31.4 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=3 ttl=111 time=34.2 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 31.367/32.913/34.156/1.158 ms
```

### 2. Analyse de trames

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | DHCP Disover| x         | `node2` `08:00:27:6b:fc:15`  | x             | `Broadcast` `FF:FF:FF:FF:FF` |
| 2     | Requête ARP|10.2.1.11       | `node1` `08:00:27:d6:00:b2` | x             | `Broadcast` `FF:FF:FF:FF:FF`   |
| 3    | DHCP Offer|10.2.1.11       | `node1` `08:00:27:d6:00:b2` | 10.2.1.3             | `node2` `08:00:27:6b:fc:15`   |
| 4    | DHCP Request|0.0.0.0      | ` 08:00:27:6b:fc:15`  | `255.255.255.255` | `Broadcast` `FF:FF:FF:FF:FF`   |
| 5    | DHCP Acknowledge|10.2.1.11       | `node1` `08:00:27:d6:00:b2` | 10.2.1.3             | `node2` `08:00:27:6b:fc:15`   |
| 6     | Requête ARP|10.2.1.11       | `node1` `08:00:27:d6:00:b2` | x             | `Broadcast` `FF:FF:FF:FF:FF`   |
| 7     | Requête ARP|0.0.0.0 | ` 08:00:27:6b:fc:15` | x  | `Broadcast` `FF:FF:FF:FF:FF`   |


