# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`        | `255.255.255.192` | 62| `10.3.0.190` | `10.3.0.191` |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126      | `10.3.0.126`         | `10.3.0.127`  |
| `server2`     | `10.3.0.192`        | `255.255.255.240` | 14                         | `10.3.0.206`         | `10.3.0.207`|

Informations du routeur : 

Ip : 
```
[adrien@router ~]$ ip a
[...]
3: enp0s8: [...]
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s8
[...]
4: enp0s9: [...]
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s9
[...]
5: enp0s10: [...]
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
[...]
```
Acces a internet et résolution de nom : 
```
[adrien@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=17.10 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 17.955/19.125/20.230/0.929 ms
[adrien@router ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 29133
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             281     IN      A       142.250.178.142

;; Query time: 16 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Mon Sep 27 12:28:44 CEST 2021
;; MSG SIZE  rcvd: 55

```

Nom : 
```
[adrien@router ~]$ hostname
router.tp3
```
Activation du routage : 
```
[adrien@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT             |



## II. Services d'infra

### 1. Serveur DHCP
La conf du serveur dhcp est dans dhcpd.conf

Marcel : 
Il a une ip dynamique
```

[adrien@marcel ~]$ ip a
[...]
2: enp0s8: [...]
    inet 10.3.0.131/26 brd 10.3.0.191 scope global dynamic enp0s8
       valid_lft 804sec preferred_lft 804sec
```

"10.3.0.131/26 brd 10.3.0.191 scope global **dynamic**"

Et il a accés a internet et un serveur dns avec des infos récupérées par le DHCP
```
[adrien@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=1a3a72a0-92d6-4bd0-bf49-afea13bf8877
DEVICE=enp0s8
ONBOOT=yes

[adrien@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=21.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=21.0 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 21.048/21.394/21.721/0.329 ms

[adrien@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 22502
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             134     IN      A       142.250.74.238

;; Query time: 20 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 10:33:11 CEST 2021
;; MSG SIZE  rcvd: 55
```

marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau
```
traceroute to google.com (142.250.179.110), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  2.496 ms  2.305 ms  2.246 ms
 2  10.0.2.2 (10.0.2.2)  2.122 ms  1.921 ms  1.875 ms
```
**_gateway (10.3.0.190)** montre qu'il passe par une gateway, qui est router.tp3, qu'on reconnait grace a son ip

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT  
| `dhcp.client1.tp3` | `10.3.0.130/26`         |          |         | `10.3.0.190/26`  |
| `marcel.client1.tp3` | `IP dynamiques`         |       |         | `10.3.0.190/26`  

### 2. Serveur DNS
#### B. SETUP copain

Nom de dns1 ainsi que son ip dans le réseau server1 : 
```
[adrien@dns1 ~]$ hostname
dns1.server1.tp3
[adrien@dns1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ef:0e:f4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.2/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feef:ef4/64 scope link
       valid_lft forever preferred_lft forever
```
La config de la carte réseau

```
[adrien@dns1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8

BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
NETMASK=255.255.255.128
IPADDR=10.3.0.2
GATEWAY=10.3.0.126
DNS1=1.1.1.1
```
Install de bind 
```
[adrien@dns1 ~]$dnf install -y bind bind-utils
```

Fichier named.conf
```
[adrien@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";

        recursion yes;
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" {
        type master;
        file "/etc/bind/zones/server1.tp3.forward";
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };
};

zone "server2.tp3" {
        type master;
        file "/etc/bind/zones/server2.tp3.forward";
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

Fichier server1.tp3.forward
```
[adrien@dns1 ~]$ sudo cat /etc/bind/zones/server1.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server1.tp3. root.server1.tp3. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum

@       IN      NS      dns1.server1.tp3.
dns1    IN      A       10.3.0.2

router  IN      A       10.3.0.126
```

Fichier server2.tp3.forward
```
[adrien@dns1 ~]$ sudo cat /etc/bind/zones/server2.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server2.tp3. root.server2.tp3. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum;

@       IN      NS      dns1.server2.tp3.
dns1    IN      A       10.3.0.2

router  IN      A       10.3.0.206
```

On start et enabled le service named (qui est notre serveur dns) 
Et on ajoute le port nécéssaire au firewall
```
[adrien@dns1 ~]$ systemctl enable --now named
[adrien@dns1 ~]$ firewall-cmd --add-service=dns --permanent;firewall-cmd --reload 
```

Tester le DNS depuis marcel.client1.tp3

On teste sur google.com
```
[adrien@marcel ~]$ dig google.com @10.3.0.2

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @10.3.0.2
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61080
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 5b8c5abec81f93d273a61bbd615b743c743dd94ad6052fc2 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             104     IN      A       142.250.179.110

;; AUTHORITY SECTION:
google.com.             172604  IN      NS      ns2.google.com.
google.com.             172604  IN      NS      ns3.google.com.
google.com.             172604  IN      NS      ns1.google.com.
google.com.             172604  IN      NS      ns4.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172604  IN      A       216.239.34.10
ns1.google.com.         172604  IN      A       216.239.32.10
ns3.google.com.         172604  IN      A       216.239.36.10
ns4.google.com.         172604  IN      A       216.239.38.10
ns2.google.com.         172604  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172604  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172604  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172604  IN      AAAA    2001:4860:4802:38::a

;; Query time: 1 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 04 23:38:05 CEST 2021
;; MSG SIZE  rcvd: 331

```

On teste sur la zone forward
```
[adrien@marcel ~]$ dig dns1.server1.tp3 @10.3.0.2

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3 @10.3.0.2
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5859
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 71ac3e3bc695f9f90ad8e2b5615b74e4d969db834c2f0cde (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.2

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 1 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 04 23:40:52 CEST 2021
;; MSG SIZE  rcvd: 103

```

A chaque fois on voit que c'est bien le serveur DNS qui répond grace a cette ligne ``;; SERVER: 10.3.0.2#53(10.3.0.2)`` , 10.3.0.2 étant l'ip du serveur DNS

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT  
| `dhcp.client1.tp3` | `10.3.0.130/26`         |          |         | `10.3.0.190/26`  |
| `marcel.client1.tp3` | `IP dynamiques`         |       |         | `10.3.0.190/26`  
| `dns1.server1.tp3` |        |   10.3.0.2/25    |         | `10.3.0.126/25`  

## 3. Get deeper
### A. DNS forwarder
Affiner la configuration du DNS
```
[...]
        recursion yes;
[...]
```

Marcel peut résoudre des noms publics comme google.com en utilisant mon serveur DNS 
```
[adrien@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57298
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 1c6eaa8b63e7111379448ccb615bf9e8717713ba1558ad89 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.214.174

;; AUTHORITY SECTION:
google.com.             172800  IN      NS      ns4.google.com.
google.com.             172800  IN      NS      ns2.google.com.
google.com.             172800  IN      NS      ns3.google.com.
google.com.             172800  IN      NS      ns1.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172800  IN      A       216.239.34.10
ns1.google.com.         172800  IN      A       216.239.32.10
ns3.google.com.         172800  IN      A       216.239.36.10
ns4.google.com.         172800  IN      A       216.239.38.10
ns2.google.com.         172800  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172800  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172800  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172800  IN      AAAA    2001:4860:4802:38::a

;; Query time: 3479 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Tue Oct 05 09:08:24 CEST 2021
;; MSG SIZE  rcvd: 331

```
Cet ligne montre que marcel passe bien par le server dns `;; SERVER: 10.3.0.2#53(10.3.0.2)`

## B. On revient sur la conf du DHCP

Le DHCP donne désormais l'adresse de mon serveur DNS aux clients
```
[adrien@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 900;
[...]
  option domain-name-servers 10.3.0.2;
}

```

Création de johnny
```
[adrien@johnny ~]$ hostname
johnny.client1.tp3
```
Et qui récupére ses informations en grace au dhcp
```
[adrien@johnny ~]$ cat /etc/sysc
sysconfig/   sysctl.conf  sysctl.d/
[adrien@johnny ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=1a3a72a0-92d6-4bd0-bf49-afea13bf8877
DEVICE=enp0s8
ONBOOT=yes
```

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT  
| `dhcp.client1.tp3` | `10.3.0.130/26`         |          |         | `10.3.0.190/26`  |
| `marcel.client1.tp3` | `IP dynamiques`         |       |         | `10.3.0.190/26`  
| `dns1.server1.tp3` |        |   `10.3.0.2/25`    |         | `10.3.0.126/25`  
| `johnny.client1.tp3` |  `IP dynamiques` |  |  | `10.3.0.190/26`

## III. Services métier
 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients
 
Serveur NGINX

```
[adrien@web1 ~]$ sudo dnf install nginx
[...]
[adrien@web1 ~]$ sudo systemctl start nginx
[adrien@web1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor prese>
   Active: active (running) since Fri 2021-10-08 13:09:05 CEST; 5min ago
 [...]
[adrien@web1 ~]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[adrien@web1 ~]$ sudo firewall-cmd --zone=public --add-service=http
success
```


Test test test et re-test
Curl depuis Marcel : 

```

[adrien@marcel ~]$ curl web1:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

[...]

      </div>
    </div>
  </body>
</html>
```
| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT  
| `dhcp.client1.tp3` | `10.3.0.130/26`         |          |         | `10.3.0.190/26`  |
| `marcel.client1.tp3` | `IP dynamiques`         |       |         | `10.3.0.190/26`  
| `dns1.server1.tp3` |        |   `10.3.0.2/25`    |         | `10.3.0.126/25`  
| `johnny.client1.tp3` |  `IP dynamiques` |  |  | `10.3.0.190/26`
| `web1.server2.tp3` |   |  | `10.3.0.194` | `10.3.0.206/28`

### 2. Partage de fichiers
#### B. Le setup wola

Setup d'une nouvelle machine, qui sera un serveur NFS
```
[adrien@nfs1 ~]$ sudo dnf -y install nfs-utils
[...]
[adrien@nfs1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[adrien@nfs1 ~]$ cat /etc/exports
/srv/nfs_share/ 10.3.0.195/28(rw,no_root_squash)
```

Configuration du client NFS
```
[adrien@web1 ~]$ sudo dnf -y install nfs-utils
[...]
[adrien@web1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[adrien@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/
[adrien@web1 ~]$ cat /etc/fstab
[...]
nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/           nfs     defaults        0 0
```

 TEEEEST
 tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3
```
[adrien@web1 ~]$ cd /srv/nfs/
[adrien@web1 nfs]$ echo "Salut à toi It4 si tu lis ce message" > message.txt
[adrien@web1 nfs]$ ls
message.txt
[adrien@web1 nfs]$ cat message.txt
Salut à toi It4 si tu lis ce message
```

vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/
```
[adrien@nfs1 ~]$ cd /srv/nfs_share/
[adrien@nfs1 nfs_share]$ ls
message.txt
[adrien@nfs1 nfs_share]$ cat message.txt
Salut à toi It4 si tu lis ce message
```


## IV. Un peu de théorie : TCP et UDP
Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP : 

SSH : TCP
HTTP : TCP
DNS : UDP
NFS : TCP

## V. El final



![](https://i.imgur.com/8A9S7th.png)


| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.0.128`        | `255.255.255.192` | 62| `10.3.0.190` | `10.3.0.191` |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126      | `10.3.0.126`         | `10.3.0.127`  |
| `server2`     | `10.3.0.192`        | `255.255.255.240` | 14                         | `10.3.0.206`         | `10.3.0.207`|


| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT  
| `dhcp.client1.tp3` | `10.3.0.130/26`         |          |         | `10.3.0.190/26`  |
| `marcel.client1.tp3` | `IP dynamiques`         |       |         | `10.3.0.190/26`  
| `dns1.server1.tp3` |        |   `10.3.0.2/25`    |         | `10.3.0.126/25`  
| `johnny.client1.tp3` |  `IP dynamiques` |  |  | `10.3.0.190/26`
| `web1.server2.tp3` |   |  | `10.3.0.194` | `10.3.0.206/28`
| `nfs1.server2.tp3` |   |  | `10.3.0.195` | `10.3.0.206/28`

